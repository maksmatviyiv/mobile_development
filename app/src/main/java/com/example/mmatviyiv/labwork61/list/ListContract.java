package com.example.mmatviyiv.labwork61.list;

import com.example.mmatviyiv.labwork61.entity.Photo;

import java.util.List;

public interface ListContract {
    interface View {
        void display(List<Photo> dataList);

    }

    interface Presenter {
        void getData();
        void itemSelected(Photo photo);
    }

    interface Model {
        List<Photo> callRetrofit();
    }
}

