package com.example.mmatviyiv.labwork61.list;

import com.example.mmatviyiv.labwork61.entity.Photo;

public interface OnItemClickListener {
    void onItemClick(Photo photo);
}
