package com.example.mmatviyiv.labwork61.network;

import com.example.mmatviyiv.labwork61.entity.Photo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    @GET("/photos")
    Call<List<Photo>> getPhotos();
}
