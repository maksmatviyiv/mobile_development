package com.example.mmatviyiv.labwork61.list;

import com.example.mmatviyiv.labwork61.ApplicationEx;
import com.example.mmatviyiv.labwork61.entity.Photo;
import com.example.mmatviyiv.labwork61.list.ListContract;
import com.example.mmatviyiv.labwork61.list.ListModel;

import java.util.List;

public class ListPresenter implements ListContract.Presenter {
    private ListContract.View mView;
    private ListContract.Model mModel;
    private List<Photo> mDataList;
    protected ApplicationEx application;

    public ListPresenter(ListContract.View mView) {
        this.mView = mView;
        this.mModel = new ListModel();
        this.application = new ApplicationEx();
    }

    @Override
    public void getData() {
        mDataList = mModel.callRetrofit();
        mView.display(mDataList);
    }
    public void itemSelected(Photo photo) {
        application.setCurrentPhoto(photo);
        application.getFragmentNavigation().showDetailsFragment();
    }
}


