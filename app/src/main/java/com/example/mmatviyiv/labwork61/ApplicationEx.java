package com.example.mmatviyiv.labwork61;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import com.example.mmatviyiv.labwork61.entity.Photo;
import com.example.mmatviyiv.labwork61.navigation.FragmentNavigation;

public class ApplicationEx extends Application {
    private Photo currentPhoto;
    private FragmentNavigation mFragmentNavigation;
    public static Context mContext;
    public Activity mActivity;
    public static String mTitle;
    public static String mThumbnailUrl;

    public static Context getContext() {
        return mContext;
    }

    public Activity getActivity() {
        return mActivity;
    }

    public static String getTitle() {
        return mTitle;
    }

    public static void setTitle(String mTitle) {
        ApplicationEx.mTitle = mTitle;
    }

    public static String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public static void setThumbnailUrl(String mThumbnailUrl) {
        ApplicationEx.mThumbnailUrl = mThumbnailUrl;
    }
    public Photo getCurrentPhoto() {
        return currentPhoto;
    }

    public void setCurrentPhoto(Photo currentPhoto) {
        this.currentPhoto = currentPhoto;
    }

    public FragmentNavigation getFragmentNavigation() {
        return mFragmentNavigation;
    }

    public void setFragmentNavigation(FragmentNavigation mFragmentNavigation) {
        this.mFragmentNavigation = mFragmentNavigation;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

}
