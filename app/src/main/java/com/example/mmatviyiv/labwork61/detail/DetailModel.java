package com.example.mmatviyiv.labwork61.detail;


import com.example.mmatviyiv.labwork61.ApplicationEx;
import com.example.mmatviyiv.labwork61.data.FavoriteDbHelper;

public class DetailModel implements DetailContract.Model {
    FavoriteDbHelper mDatabaseHelper;

    public DetailModel() {
        this.mDatabaseHelper = new FavoriteDbHelper(ApplicationEx.getContext());
    }

    public boolean addToFavorites(String title) {
        mDatabaseHelper.addToFavorites(ApplicationEx.getTitle());
        return true;
    }

    public void deleteFromFavourite(String title) {
        mDatabaseHelper.deleteFromFavourite(ApplicationEx.getTitle());
    }

}
